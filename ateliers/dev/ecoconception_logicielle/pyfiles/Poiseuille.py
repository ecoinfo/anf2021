#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 10 19:26:27 2019

@author: chauchat
"""
import math
import numpy as np

def analytic_poiseuille2D(Ly,nu,dpdx,X,Y):
    Upois = dpdx*Ly**2/(12.*nu)*6*(1/4.-Y**2)
    return Upois


def analytic_poiseuille1D(Ly,nu,dpdx,Y):
    Upois = dpdx*Ly**2/(2.*nu)*(1/4.-Y**2)
    return Upois


def analytic_poiseuille2D_rect(Lx,Ly,nu,dpdx,X,Y,N,M):
    Upois=np.zeros((N,M))
    B=Lx/Ly
    for i in range(0,N):	#only internal nodes 
        for j in range(0,M): 
            dum = 0.
            kmax = 100
            eps = 1e-6
            k = 0
            while (k < kmax) :
                dum = dum + (-1)**(k+1) * math.cosh((2*k+1)*math.pi*X[i,j]) \
                                        * math.cos((2*k+1)*math.pi*Y[i,j]) \
                                 / ((2*k+1)**3 *math.cosh((k+1./2.)*math.pi*B))
                k=k+1
            Upois[i,j] = 6*(1/4.-Y[i,j]**2) + 48.* dum /math.pi**3 
    # rescaling
    Upois = dpdx*Ly**2/(12.*nu)*Upois        
    return Upois