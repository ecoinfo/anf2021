import numpy as np
import math
import pylab as pylab
import matplotlib.pyplot as plt
import Poiseuille as pois
from timeit import default_timer as timer


def RMS_E(X1, X2):
    rms = math.sqrt(sum((X1 - X2) ** 2) / np.size(X1))
    return rms

start = timer()
#
#
#     INPUT PARAMETERS
#
# Domain length
Lx = 1.0

# mesh cells in x-direction
M = 2048

# Grid sizes
dx = Lx / (M - 1)

# Pressure gradient
dpdx = 1.0

# Reynolds number: Re = U L / nu
Re = 1.0

# Diffusivity
Gamma = 1.0 / Re

# Velocity
c0 = 0.1

# Time step:
dt = 0.1

# Configuration
case = "diffusion"
#case = "convection"

#
#     INITIALISATION
#

#
x = np.linspace(-Lx / 2, Lx / 2, M)

# Initial velocity vector field
uo = np.zeros([M], dtype=np.float)

if (case == "convection"):
    # Pressure gradient
    dpdx = 0.0
    # Velocity
    c0 = 0.1
    # Diffusivity
    Gamma = 0
    uo = 1.0 / (np.sqrt(2 * 0.05) * np.pi) * np.exp(-x ** 2 / (2 * 0.05 ** 2))
    # Time step:
    dt = 0.1
    # Maximum number of iterations
    maxIter = 1.5 / dt
elif (case == "diffusion"):
    # Pressure gradient
    dpdx = 1.0
    # Velocity
    c0 = 0.0
    # Diffusivity
    Gamma = 1.0 / Re
    # Time step:
    dt = 0.1
    # Maximum number of iterations
    maxIter = 1000

# Precision
eps = 1e-9

#
# Print initial conditions:
print("case : ",case);
print("M  : ", M);
print("dx : ", dx);
print("dt : ", dt);
print("eps: ", eps);


# boundary conditions
BCNorth = 0
BCSouth = 0

# Initialisation of variables
print("Matrice M x M: ", M*M*8/1024, " kb");

u    = np.zeros([M], dtype=np.float)
uold = np.zeros([M], dtype=np.float)
Au = np.zeros([M, M], dtype=np.float)
Su = np.zeros([M], dtype=np.float)

uold = uo
u    = uo

# Min step check:
last_rms = 1.0
min_step = eps / 10.0

# First iteration
k = int(0)
while k < maxIter:
    # Predictor step
    for j in range(1, M - 1):  # only internal nodes
        #
        # Convection fluxes
        #
        Fe = c0
        Fw = c0
        #
        # Diffusion fluxes
        #
        De = Gamma / dx
        Dw = Gamma / dx
        #
        # Compute system coefficients
        #
        aE = De + max(-Fe, 0)
        aW = Dw + max(Fw, 0)
        #        aP = De + Dw + max( Fe,0) + max(-Fw,0)
        aP = aE + aW + Fe - Fw
        #
        # Assemble matrix coefficients
        #
        Au[j, j - 1] = -aW
        Au[j, j] = dx / dt + aP
        Au[j, j + 1] = -aE
        Su[j] = dx / dt * uold[j] + dpdx * dx

    # Boundary conditions
    # North boundary
    if (BCNorth == 0):  # Dirichlet
        Au[M - 1, M - 1] = 1
        Su[M - 1] = 0
        u[M - 1] = 0
    elif (BCNorth == 1):  # Neumann
        Au[M - 1, M - 1] = 1
        Au[M - 1, M - 2] = -1
        Su[M - 1] = 0
        u[M - 1] = u[M - 2]
    # South boundary
    if (BCSouth == 0):  # Dirichlet
        Au[0, 0] = 1
        Su[0] = 0
        u[0] = 0
    elif (BCSouth == 1):  # Neumann
        Au[0, 0] = 1
        Au[0, 1] = -1
        Su[0] = 0
        u[0] = u[1]

    # Solve algebraic system
    u = np.linalg.solve(Au, Su)

    # Compute rms error at the end of each time step
    rms_u = RMS_E(u, uold)

    if (rms_u < eps):
        print("The simulation has converged in ", k, " iterations")
        break
    elif (abs(last_rms - rms_u) < min_step):
        print("The simulation convergence is limited to ", rms_u, " in ", k, " iterations\n")
        break
    else:
        print("k=", k, ": RMS U =", rms_u)
    uold = u
    k = k + 1
    last_rms = rms_u

endtime = timer()
print("Time in seconds = ", endtime - start)  # Time in seconds, e.g. 5.38091952400282

print("result samples:")
for idx in range(0, M, int(M / 10)):
    print("u[", idx, "] = ", u[idx]);

##############################################################################
#
#      Plot solutions
#
##############################################################################
f = pylab.figure(1, figsize=(12, 8))
pylab.plot(u, x, "or", label="Numeric")
if case == "diffusion":
    Upois = pois.analytic_poiseuille1D(Lx, 1. / Re, dpdx, x)
    pylab.plot(Upois, x, "--k", label="Analytic")
if case == "convection":
    # progressive wave
    xmid = c0 * k * dt
    uf = np.zeros([M], dtype=np.float)
    # uf[np.where(np.logical_and(x>=xmid-0.1,x<=xmid+0.1))] = 1
    uf = 1. / (np.sqrt(2 * 0.05) * np.pi) * np.exp(-(x - xmid) ** 2 / (2 * 0.05 ** 2))
    pylab.plot(uo, x, ":k", label="Initial condition")
    pylab.plot(uf, x, "--k", label="Analytic t")
pylab.legend()
pylab.axis([0, max([np.max(u) * 1.1, np.max(uo) * 1.1]), -Lx / 2, Lx / 2])
pylab.ylabel(r"$x$", fontsize=12)
pylab.xlabel(r"$u$", fontsize=12)
pylab.show()
