#!/bin/bash

DO_ALL=0

# Use US Locale:
export LANG=C



# Fortran
#./run-bench.sh otherfiles/1Dfortran 


if [ "$DO_ALL" -eq "1" ] ; then

# Python (slow, disabled by default)
./run-bench.sh pyfiles/main1D_cbv1.py
./run-bench.sh pyfiles/main1D_cbv2.py
fi

# Python
./run-bench.sh pyfiles/main1D_cbv3.py
./run-bench.sh pyfiles/main1D_cbv4.py


# Julia
source jlfiles/env.sh
./run-bench.sh jlfiles/main1D_cb.jl
./run-bench.sh jlfiles/main1D_cbv2.jl
./run-bench.sh jlfiles/main1D_cbv3.jl
./run-bench.sh jlfiles/main1D_opt.jl


echo "That's All, folks !!!"

