#!/bin/bash

DO_VMSTAT=0

# TRAP: Do not leave children jobs running if the shell has been cancelled
cleanup_trap() {
    CHILDREN_PIDS=$(jobs -p)
    if [ -n "$CHILDREN_PIDS" ]
    then
        trap - EXIT
        echo -e "SHELL cancelled, stopping $CHILDREN_PIDS"
        # we may try to send only TERM before a pause and a last loop with KILL signal ?
        kill $CHILDREN_PIDS

        echo -e "SHELL cancelled, waiting on $CHILDREN_PIDS"
        # wait for all pids
        for pid in $CHILDREN_PIDS; do
            wait $pid
        done

        CHILDREN_PIDS=$(jobs -p)
        if [ -n "$CHILDREN_PIDS" ]
        then
            echo -e "SHELL cancelled, killing $CHILDREN_PIDS"
            kill -9 $CHILDREN_PIDS
        fi
  fi
}
trap cleanup_trap EXIT


# HERE BEGINS THE SCRIPT

# Use US Locale:
export LANG=C

if [ -z "$1" ]
  then
    echo "No argument 1 (CMD) supplied"
    exit 1
fi


processId="1D" # all benchmarks contains '1D"

# Check if service is running:
PID=`ps -ef | grep -i "${processId}" | grep -v "grep" | grep -v "\-bench\.sh" | awk '{print $2}'`
#echo "PID: '${PID}'"

N_PID=`echo "${PID}" | wc -w`

if [ "${N_PID}" -ne "0" ]
then
    echo "Process '${processId}' (PID = ${PID}) is already running ..."
    exit 1
fi



# get CPU infos (settings):
if [ -z "${CPU_CORES}" ]
then
    source ./bench-settings.sh

    echo "Using CPU_CORES = $CPU_CORES"
fi


CMD="$1"
LABEL=`basename $1`
HOSTNAME=`hostname`

EXT=${LABEL##*.}
case $EXT in
  py)
    CMD="python3 $CMD"
    ;;
  jl)
    CMD="julia $CMD"
    ;;
  *)
    ;;
esac


dir=./results/${HOSTNAME}-$(date +"%Y_%m_%d")/${LABEL}
mkdir -p $dir
echo "Store results in $dir"

logExt=txt
vmFile=${dir}/vm_${LABEL}.${logExt}
logFile=${dir}/result_${LABEL}


echo "---------------------------------------"
echo "Run benchmarks on command '$CMD' on $CPU_CORES CPU cores"
echo "---"


if [ $DO_VMSTAT -eq 1 ]
then
    # stop any vmstat
    killall -q vmstat

    echo "Start vmstat (1s) in background (CPU 0)..."
    taskset -c 0 vmstat -t 1 2>&1 > ${vmFile} &
fi



# Single job:
echo "---"
echo "Run Benchmark on 1 CPU (monothread) ... '$CMD'"

# Start monothread:

echo "Run process (CPU 1)..."
taskset -c 1 $CMD > ${logFile}_single.${logExt} 2>&1

echo "Results (monothread):"
grep -h "Time in seconds" ${logFile}_single.${logExt}

sleep 2


# Parallel jobs (1 per CPU core):
echo "---"
echo "Run Benchmarks on $CPU_CORES CPUs (parallel) ... '$CMD'"

# Start parallel:

MAX_CORES=$((CPU_CORES-1))

for CORE in $(seq 0 $MAX_CORES)
do
    i=$((CORE+1))
    echo "Spawn process ${i} (CPU $CORE))"
    nohup taskset -c $CORE $CMD < /dev/null > ${logFile}_core_${i}.${logExt} 2>&1 &
done

echo "Waiting ..."

# Wait for runner to end properly:
PID=`ps -ef | grep "$CMD" | grep -v "grep" | grep -v "-bench.sh" | awk '{print $2}'`
N_PID=`echo "${PID}" | wc -w`

while [ "${N_PID}" -ne "0" ]; do
    sleep 1
    PID=`ps -ef | grep "$CMD" | grep -v "grep" | grep -v "-bench.sh" | awk '{print $2}'`
    N_PID=`echo "${PID}" | wc -w`
done

echo "All Processes terminated."

echo "Results (parallel):"
grep -h "Time in seconds" ${logFile}_core_*

echo "---"



if [ $DO_VMSTAT -eq 1 ]
then
    # stop any vmstat
    killall -q vmstat
fi

trap - EXIT
echo "Done."
echo "---------------------------------------"

