#!/bin/bash

# used to limit the number of cpu cores in action:

export CPU_COUNT=1 # laptop / pi4
#export CPU_COUNT=2 # g5k

export CORES_PER_CPU=4 # laptop / pi4
#export CORES_PER_CPU=16 # g5k


#echo "CPU_COUNT: $CPU_COUNT"
#echo "CORES_PER_CPU: $CORES_PER_CPU"

export CPU_CORES=$((CPU_COUNT * CORES_PER_CPU))

echo "Set CPU_CORES = $CPU_CORES"

