module func
integer, parameter :: dp = kind(1.0d0)
contains
function TDMAsolver(a, b, c, d, nf)
    ! Tri Diagonal Matrix Algorithm(a.k.a Thomas algorithm) solver
    !TDMA solver, a b c d can be NumPy array type or Python list type.
    !refer to http://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm
    !
    implicit none
    integer, intent(in) :: nf
    real(kind=dp), dimension(nf-1), intent(in) :: a, c
    real(kind=dp), dimension(nf), intent(in) :: b, d
    real(kind=dp), dimension(nf) :: TDMAsolver
    real(kind=dp), allocatable :: bc(:), dc(:)
    integer :: it
    real(kind=dp) :: mc
    allocate(bc(nf), dc(nf))
    bc = b
    dc = d
    do it=2,nf
        mc = a(it-1) / bc(it-1)
        bc(it) = bc(it) - mc *  c(it - 1)
        dc(it) = dc(it) - mc * dc(it - 1)
    enddo
    TDMAsolver = bc
    TDMAsolver(nf) = dc(nf) / bc(nf)

    do it=nf-1,1,-1
        TDMAsolver(it) = (dc(it) - c(it) * TDMAsolver(it + 1)) / bc(it)
    enddo
    deallocate(bc, dc)
end function


function RMS_E(X1, X2, sizeX1)
    implicit none
    integer, intent(in) :: sizeX1
    real(kind=dp), dimension(sizeX1), intent(in) :: X1, X2
    real(kind=dp) :: RMS_E
    RMS_E = sqrt(sum((X1 - X2)**2) / sizeX1)
end function
end module

program undfor
use func
implicit none
real(kind=dp) :: Lx, dx, dpdx, Re, Gammaa, c0, dt, Fe, De, Fw, eps, Dw, aE, aW
real(kind=dp) :: aP, rms_u, pi, last_rms, min_step
integer :: M, casee, BCNORTH, BCSOUTH, maxiter, k, j
real(kind=dp), allocatable :: Aua(:), Aub(:), Auc(:), Aud(:), u(:), uo(:)
real(kind=dp), allocatable :: uold(:), x(:)
real endtime, dtime, t(2)

endtime = dtime(t)
!
!     INPUT PARAMETERS
!
!pi value
pi = 4.0*atan(1.d0)

! Domain length
Lx = 1.0

! mesh cells in x-direction
M = 2048 * 2048

! Grid sizes
dx = Lx / (M - 1)

! Pressure gradient
dpdx = 1.0

! Reynolds number: Re = U L / nu
Re = 1.0

! Diffusivity
Gammaa = 1.0 / Re

! Velocity
c0 = 0.1

! Time step:
dt = 0.1

! Configuration
casee = 0 !'diffusion'
!casee = 1 !'convection'

!
!     INITIALISATION
!


! Initial velocity vector field
allocate(Aua(M-1), Auc(M-1))
allocate(Aub(M), Aud(M), u(M), uo(M), uold(M), x(M))

uo(:) = 0.0

if (casee == 1) then
    do j=1,M
        x(j) = -Lx / 2 + (j - 1) * Lx / M
    enddo
    ! Pressure gradient
    dpdx = 0.0
    ! Velocity
    c0 = 0.1
    ! Diffusivity
    Gammaa = 0
    uo = 1.0 / (sqrt(2 * 0.05) * pi) * exp(-x**2 / (2 * 0.05**2))
    ! Time step:
    dt = 0.1
    ! Maximum number of iterations
    maxIter = 1000  ! 1.5/dt
else if (casee == 0) then
    ! Pressure gradient
    dpdx = 1.0
    ! Velocity
    c0 = 0.0
    ! Diffusivity
    Gammaa = 1.0 / Re
    ! Time step:
    dt = 0.1
    ! Maximum number of iterations
    maxIter = 1000
end if

! Precision
eps = 1e-9

!
! Print initial conditions:
write(*,*) 'case : ',casee
write(*,*) 'M  : ', M
write(*,*) 'dx : ', dx
write(*,*) 'dt : ', dt
write(*,*) 'eps: ', eps


! boundary conditions
BCNorth = 0
BCSouth = 0

! Initialisation of variables
u(:)    = 0.0
uold(:) = 0.0

Aua(:) = 0.0
Aub(:) = 0.0
Auc(:) = 0.0
Aud(:) = 0.0


uold = uo
u    = uo

!
! Convection fluxes
!
Fe = c0
Fw = c0
!
! Diffusion fluxes
!
De = Gammaa / dx
Dw = Gammaa / dx
!
! Compute system coefficients
!
aE = De + max(-Fe, 0.0)
aW = Dw + max( Fw, 0.0)
aP = aE + aW + Fe - Fw
!
! Assemble matrix coefficients
!
Aua(1:M - 1) = -aW
Aub(2:M - 1) = dx / dt + aP
Auc(1:M - 1) = -aE

! Min step check:
last_rms = 1.0
min_step = eps / 10.0

! First iteration
k = 0
do while (k < maxIter)
    !Predictor step (input : c0, Gamma, dx, dt, uold, dpdx; output : Aua, Aub, Auc, Aud)
    Aud(:) = dx/dt * uold(:) + dpdx * dx

    ! Boundary conditions
    ! North boundary
    if (BCNorth == 0) then ! Dirichlet
        u(M) = 0
        Aub(M) = 1
        Aud(M) = 0
    else if (BCNorth == 1) then ! Neumann
        u(M) = u(M-1)
        Aua(M) = -1
        Aub(M) = 1
        Aud(M) = 0
    end if
    ! South boundary
    if (BCSouth == 0) then ! Dirichlet
        u(1) = 0
        Aub(1) = 1
        Aud(1) = 0
    else if (BCSouth == 1) then ! Neumann
        u(1) = u(2)
        Auc(1) = -1
        Aub(1) = 1
        Aud(1) = 0
    end if

    ! Solve algebraic system
    u = TDMAsolver(Aua, Aub, Auc, Aud, M)

    ! Compute rms error at the end of each time step
    rms_u = RMS_E(u, uold, M)

    if (rms_u < eps) then
        write(*,*) 'The simulation has converged in ', k, ' iterations'
        exit
    else if (abs(last_rms - rms_u) < min_step) then
        write(*,*) 'The simulation convergence is limited to ', rms_u, ' in ', k, ' iterations'
        exit
    else
        write(*,*) 'k=',k,': RMS U =', rms_u
    end if
    uold = u
    k = k + 1
    last_rms = rms_u
end do

endtime = dtime(t)
write(*,*) 'Time in seconds = ', endtime  ! Time in seconds, e.g. 5.38091952400282

write(*,*) 'result samples:'
do k=1,M,M/10
    write(*,*) 'u[',k,'] = ', u(k)
enddo

deallocate(Aua, Aub, Auc, Aud, u, uo, uold, x)

end program undfor
