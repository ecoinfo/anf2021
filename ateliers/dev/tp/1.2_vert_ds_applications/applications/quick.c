#define MAX 300000
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int tableau[MAX];

void quick_sort (int *a, int n) {
    if (n < 2)
        return;
    int p = a[n / 2];
    int *l = a;
    int *r = a + n - 1;
    while (l <= r) {
        if (*l < p) {
            l++;
        }
        else if (*r > p) {
            r--;
        }
        else {
            int t = *l;
            *l = *r;
            *r = t;
            l++;
            r--;
        }
    }
    //printf ("Etape %d\n",n);

    quick_sort(a, r - a + 1);
    quick_sort(l, a + n - l);
}
 
int main () 
{
  int i;
  for (i=0;i<MAX;i++) 
    {
      tableau[i]=rand()*i;
      // printf("--> Remplissage  tableau case : %d  valeur : %d\n",i,tableau[i]);
    }
  
  sleep(3);
  printf("Début du tri\n");
  
  quick_sort(tableau, MAX);
  
  printf("Fin du tri\n");
  
  for (i=0;i<MAX;i++) printf("%d\n",tableau[i]);
  
  return 0;
}

