# Use make to clean and create docker image:
make clean
make
# or make force-build (to clean cache)
make force-build

# Run docker image:
docker images
docker run <imageId>

# Access to docker container:
docker ps
docker exec -it <containerId> bash

