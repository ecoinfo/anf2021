#!/bin/bash
DO_ALL=1

CURRENT=`pwd`

set -eux ;

if [ "$DO_ALL" -eq "1" ] ; then

# LL TP:
cd $CURRENT/tp/1.2_vert_ds_applications/wattmetres/
#stdbuf -oL ./wattsup.bin -t -c 10000 ttyUSB0 watts > test_wattsup.txt
# wattsup: [error] ttyUSB0: No such file or directory

cd $CURRENT/tp/1.2_vert_ds_applications/scripts/
./cpuburn.sh 10
#./hdparm.sh 5
# /dev/sda: No such file or directory
./iperf-server.sh 10
./iperf-client.sh 10 localhost

cd $CURRENT/tp/1.2_vert_ds_applications/applications/
./clean.sh
./compile.sh
./bubble1_dumb
./quick_lent
./quick
./calcul_pi
./lecture_pi


# CB TP:
# fortran
cd $CURRENT/ecoconception_logicielle/otherfiles/
make
./1Dfortran

# julia
cd $CURRENT/ecoconception_logicielle/jlfiles/

export PATH=/opt/julia-1.1.1/bin:$PATH
julia --version
julia main1D_cb.jl
julia main1D_cbv2.jl
julia main1D_cbv3.jl
julia main1D_opt.jl

# python
cd $CURRENT/ecoconception_logicielle/pyfiles/
python3 --version
python3 main1D.py
python3 main1D_cb.py
python3 main1D_cbv1.py
python3 main1D_cbv2.py
# v3 disabled (19.9.17): not working anymore (pip install failed)
# pythran: run twice:
# python3 main1D_cbv3.py
# python3 main1D_cbv3.py
# numba
python3 main1D_cbv4.py
python3 main1D_cbv4_plot.py # no plot: x server not in docker

fi

echo "Test done"

