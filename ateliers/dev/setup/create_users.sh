#!/bin/bash
addgroup docker

# Users anf 1 to 6:
addgroup anf

for i in $(seq 6)
do
USER=anf$i
echo "create user $USER"
adduser --shell /bin/bash $USER
usermod -aG anf $USER
usermod -aG docker $USER
done

mkdir /home/anf
chown -R radmin:anf /home/anf

