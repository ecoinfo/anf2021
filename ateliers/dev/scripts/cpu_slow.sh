#!/bin/bash
# Needs sudo

FREQ="800MHz" # nominal
echo "set CPU frequency to $FREQ ..."

cpupower -c all frequency-set -d $FREQ -u $FREQ -g powersave
cpupower -c all frequency-info

